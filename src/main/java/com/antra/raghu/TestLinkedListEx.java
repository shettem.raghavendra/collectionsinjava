package com.antra.raghu;

import java.util.LinkedList;

public class TestLinkedListEx {

	public static void main(String[] args) {
		LinkedList<String> list=new LinkedList<String>();  
		System.out.println("Initial list of elements: "+list);  
		list.add("Ravi");  
		list.add("Vijay");  
		list.add("Ajay");  
		System.out.println("After invoking add(E e) method: "+list);  

		//Adding an element at the specific position  
		list.add(1, "Gaurav");  
		System.out.println("After invoking add(int index, E element) method: "+list);  

		LinkedList<String> ll2=new LinkedList<String>();  
		ll2.add("Sonoo");  
		ll2.add("Hanumat");  

		//Adding second list elements to the first list  
		list.addAll(ll2);  
		System.out.println("After invoking addAll(Collection<? extends E> c) method: "+list);  
		LinkedList<String> ll3=new LinkedList<String>();  
		ll3.add("John");  
		ll3.add("Rahul");  

		//Adding second list elements to the first list at specific position  
		list.addAll(1, ll3);  
		System.out.println("After invoking addAll(int index, Collection<? extends E> c) method: "+list);  

		//Adding an element at the first position  
		list.addFirst("Lokesh");  
		System.out.println("After invoking addFirst(E e) method: "+list);  

		//Adding an element at the last position  
		list.addLast("Harsh");  
		System.out.println("After invoking addLast(E e) method: "+list);  

		//Removing specific element from arraylist  
		list.remove("Vijay");  
		System.out.println("After invoking remove(object) method: "+list);   
		//Removing element on the basis of specific position  
		list.remove(0);  
		System.out.println("After invoking remove(index) method: "+list);   
		
		LinkedList<String> ll4=new LinkedList<String>();  
		ll4.add("Ravi");  
		ll4.add("Hanumat");  
		
		// Adding new elements to arraylist  
		list.addAll(ll4);  
		System.out.println("Updated list : "+list);   
		
		//Removing all the new elements from arraylist  
		list.removeAll(ll4);  
		System.out.println("After invoking removeAll() method: "+list);   
		
		//Removing first element from the list  
		list.removeFirst();  
		System.out.println("After invoking removeFirst() method: "+list);  
		
		//Removing first element from the list  
		list.removeLast();  
		System.out.println("After invoking removeLast() method: "+list);  
		
		//Removing first occurrence of element from the list  
		list.removeFirstOccurrence("Gaurav");  
		System.out.println("After invoking removeFirstOccurrence() method: "+list);  
		
		//Removing last occurrence of element from the list  
		list.removeLastOccurrence("Harsh");  
		System.out.println("After invoking removeLastOccurrence() method: "+list);  

		//Removing all the elements available in the list       
		list.clear();  
		System.out.println("After invoking clear() method: "+list);   
	}  
}

