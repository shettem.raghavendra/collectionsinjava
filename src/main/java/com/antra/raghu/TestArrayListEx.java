package com.antra.raghu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class TestArrayListEx {

	public static void main(String[] args) {
		List<Integer> al = new ArrayList<>();
		al.add(10);
		al.add(200);
		al.add(34);
		al.add(80);
		al.add(176);
		
		System.out.println(al);
		
		al.remove(4);
		
		System.out.println(al);
		
		//---------------------------------------
		List<Integer> subList = new ArrayList<>();
		subList.add(39);
		subList.add(55);
		
		al.addAll(subList);
		
		System.out.println(al);
		
		if(al.contains(34)) {
			System.out.println("ELEMENT 34 PRESENT");
		}
		System.out.println(al.get(1));
		
		//read items using iterator
		Iterator<Integer> itr =  al.iterator();
		
		while (itr.hasNext()) {
			Integer element =  itr.next();
			System.out.println(element);
		}
		
		//replace old value with new value at given index
		al.set(1,400);
		System.out.println(al);
		
		al.retainAll(subList);
		System.out.println(al);
		
		int index = al.indexOf(55);
		System.out.println(index);
		
		//Array To List
		List<Integer> list = Arrays.asList(10,20,34,55);
		//List to Array;
		Integer[] arr = list.toArray(new Integer[] {});

		//-------------------------
		al.clear();
		System.out.println(al.isEmpty());
	}
}
